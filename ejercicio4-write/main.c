#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

#define BUF_SIZE 256

int main(void){

    char entrada[30];
    syscall(SYS_write, 1, "Ingrese el texto", 30);
    int leido = read(0, entrada, sizeof entrada);
    entrada[leido] = '\0';

    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);

    return 0;
}

