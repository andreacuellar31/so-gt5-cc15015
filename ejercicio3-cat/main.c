#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

#define BUF_SIZE 256

int main(void){

    int archivo, entrada;
    char buf[BUF_SIZE];

    archivo = open("ejercicio3.txt", O_RDONLY);
    entrada = read(archivo, buf, BUF_SIZE);
    syscall(SYS_write, 1, buf, strlen(buf));
    return 0;
}

