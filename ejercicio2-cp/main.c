#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#define MODE 0666
#define BUF_SIZE 256

void main(){
    int origen, copia, entrada, salida;
    char buf[BUF_SIZE];

    origen = open("ejercicio2.txt", O_RDONLY);
    copia = creat("ejercicio2copy.txt", MODE);
    entrada = read(origen, buf, BUF_SIZE);
    salida = write(copia, buf, entrada);
    exit(0);
}

